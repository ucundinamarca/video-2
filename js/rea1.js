/**
 * @author Edilson Laverde Molina
 * @email edilsonlaverde_182@hotmail.com
 * @create date 2020-12-12 18:45:48
 * @modify date 2020-12-18 09:48:07
 * @desc Videos REA3
 */
var videos = [{
    "id": "f0yOGoBi1_k",
    "titulo": "",
    "descripcion": "",
    "url": "f0yOGoBi1_k",
    "preguntas": [
        {
            "second": "35",
            "question": "¿Estás de acuerdo?",
            "answers": ["Si","No"]
        }, {
            "second": "51",
            "question": "¿Estás de acuerdo?",
            "answers": ["Si","No"]
        }, {
            "second": "70",
            "question": "¿Sabías esto?",
            "answers": ["Si","No"]
        }, {
            "second": "112",
            "question": "¿Conocías estos tipos de pobrezas?",
            "answers": ["Si","No"]
        }, {
            "second": "139",
            "question": "¿A usted le es difícil o es fácil cambiar?",
            "answers": ["Si","No"]
        }
    ]
}
];    

